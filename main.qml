import QtQuick 2.3
import QtQuick.Controls 1.2

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    menuBar: MenuBar {
        Menu {
            title: qsTr("File")
            MenuItem {
                text: qsTr("&Open")
                onTriggered: console.log("Open action triggered");
            }
            MenuItem {
                text: qsTr("Exit")
                onTriggered: Qt.quit();
            }
        }
    }

    Label {
        text: qsTr("TimeManager")
        anchors.verticalCenterOffset: -424
        anchors.horizontalCenterOffset: -23
        anchors.centerIn: parent
    }

    Button {
        id: button1
        x: 180
        y: 181
        width: 100
        height: 50
        text: qsTr("Start / Stop")
    }

    TableView {
        id: tableView1
        x: 322
        y: 181
        width: 191
        height: 100
    }

    Text {
        id: text1
        x: 39
        y: 181
        text: qsTr("Driving Time")
        font.pixelSize: 12
    }

    CheckBox {
        id: checkBox1
        x: 39
        y: 264
        width: 87
        height: 17
        text: qsTr("Enable GPS")
    }

    Button {
        id: button2
        x: 180
        y: 311
        width: 100
        height: 50
        text: qsTr("Start / Stop")
    }

    TableView {
        id: tableView2
        x: 322
        y: 311
        width: 191
        height: 100
    }

    Text {
        id: text2
        x: 39
        y: 311
        width: 87
        height: 14
        text: qsTr("Working Time")
        font.pixelSize: 12
    }

    CheckBox {
        id: checkBox2
        x: 39
        y: 394
        width: 87
        height: 17
        text: qsTr("Merge shorter breaks")
        checked: true
    }

    Button {
        id: button3
        x: 180
        y: 231
        width: 100
        height: 50
        text: qsTr("Manual edit")
    }

    Button {
        id: button4
        x: 180
        y: 361
        width: 100
        height: 50
        text: qsTr("Manual edit")
    }

    Button {
        id: button5
        x: 39
        y: 102
        width: 100
        height: 50
        text: qsTr("<")
    }

    Button {
        id: button6
        x: 413
        y: 102
        width: 100
        height: 50
        text: qsTr(">")
    }

    Label {
        x: -8
        y: 7
        text: qsTr("Date")
        anchors.centerIn: parent
        anchors.verticalCenterOffset: -397
        anchors.horizontalCenterOffset: -23
    }
}
